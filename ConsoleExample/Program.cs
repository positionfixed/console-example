﻿using BrowseEmAll.API;
using BrowseEmAll.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleExample
{
    class Program
    {
        // For a console application 'Standalone' is important so all browser run 
        // in a seperate process
        static BrowserManager manager = new BrowserManager(IntegrationMode.Standalone);

        static int browserId;

        static void Main(string[] args)
        {
            // Adding the different events
            manager.OnBrowserReady += Manager_OnBrowserReady;
            manager.OnNavigateComplete += Manager_OnNavigateComplete;
            manager.OnDocumentComplete += Manager_OnDocumentComplete;
            
            Console.WriteLine("Starting Chrome browser...");
            browserId = (int)manager.OpenBrowser(Browser.CHROME48, new BrowserSettings());

            Console.ReadKey();

            manager.CloseBrowser(browserId);
        }

        private static void Manager_OnDocumentComplete(object sender, BrowseEmAll.API.Events.DocumentCompletedEventArgs e)
        {
            // Loading of the page complete
            Console.WriteLine("Document Completed!");
        }

        private static void Manager_OnNavigateComplete(object sender, BrowseEmAll.API.Events.NavigationCompleteEventArgs e)
        {
            // Browser has finished navigating
            Console.WriteLine("Navigated to " + e.Url);
        }

        private static void Manager_OnBrowserReady(object sender, BrowseEmAll.API.Events.BrowserReadyEventArgs e)
        {
            // The browser is now ready to receive input
            Console.WriteLine("Browser Ready!");
            manager.Navigate(browserId, "http://www.google.de");
        }
    }
}
